/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol4D;

import java.util.LinkedList;

/**
 *
 * @author eljul
 */
public class Funcion extends Nodo4D{

    public String Nombre;
    public LinkedList<Nodo4D> Contenido;

    public Funcion(String Nombre, LinkedList<Nodo4D> Contenido) {
        this.Nombre = Nombre;
        this.Contenido = Contenido;
        Contenido.add(new Retorno());
    }
   
    @Override
    public double Ejecutar() {
        return 0.0;
    }
    
}

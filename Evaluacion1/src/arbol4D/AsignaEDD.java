/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol4D;

/**
 *
 * @author jarango
 */
public class AsignaEDD extends Nodo4D{
    
    public enum TipoEDD {
        STACK,
        HEAP
    }
    
    public TipoEDD tipo;
    public Nodo4D direccion;
    public Nodo4D resultado;

    public AsignaEDD(TipoEDD tipo, Nodo4D direccion, Nodo4D resultado) {
        this.tipo = tipo;
        this.direccion = direccion;
        this.resultado = resultado;
    }
    
    
    
    @Override
    public double Ejecutar() {
        switch(tipo){
            case HEAP:
                Double pos = direccion.Ejecutar();
                Double res = ContenedorEstatico.PopHeap(pos.intValue());
                resultado.valAsignar = res;
                resultado.Ejecutar();
                break;
            case STACK:
                pos = direccion.Ejecutar();
                res = ContenedorEstatico.PopStack(pos.intValue());
                resultado.valAsignar = res;
                resultado.Ejecutar();
                break;
        }
        return 0.0;
    }
    
}

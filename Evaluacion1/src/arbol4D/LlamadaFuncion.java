/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol4D;

/**
 *
 * @author eljul
 */
public class LlamadaFuncion extends Nodo4D{

    public String id;

    public LlamadaFuncion(String id) {
        this.id = id;
    }
    
    
    @Override
    public double Ejecutar() {
        for (NodoAuxiliar actual : ContenedorEstatico.Auxiliar) {
            if(actual.Nombre.equalsIgnoreCase(id)){
                ContenedorEstatico.Incremento = false;
                ContenedorEstatico.PilaRetornos.push(ContenedorEstatico.LineaActual);
                ContenedorEstatico.LineaActual = actual.posicion;
                return 0.0;
            }
        }
        return 0.0;
    }
    
}

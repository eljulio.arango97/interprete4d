/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol4D;

import evaluacion1.Formulario;

/**
 *
 * @author eljul
 */
public class Asignacion extends Nodo4D{

    public static enum TipoAsignacion{
        STACK,
        HEAP,
        TEMP,
        PTRS,
        PTRH
    }
    
    public TipoAsignacion tipo;
    public Nodo4D Direccion1;
    public Nodo4D Direccion2;

    public Asignacion(TipoAsignacion tipo, Nodo4D Direccion1, Nodo4D Direccion2) {
        this.tipo = tipo;
        this.Direccion1 = Direccion1;
        this.Direccion2 = Direccion2;
    }
    
    public Asignacion(TipoAsignacion tipo, Nodo4D Direccion1) {
        this.tipo = tipo;
        this.Direccion1 = Direccion1;
        this.Direccion2 = null;
    }
    
    String id;
    
    public Asignacion(TipoAsignacion tipo, Nodo4D Direccion1, String id) {
        this.tipo = tipo;
        this.Direccion1 = Direccion1;
        this.Direccion2 = null;
        this.id = id;
    }
    
    
    @Override
    public double Ejecutar() {
        switch(tipo){
            case HEAP:
                Double posicion = Direccion1.Ejecutar();
                Double valor = Direccion2.Ejecutar();
                ContenedorEstatico.InsertarEnHeap(posicion.intValue(), valor);
                break;
            case STACK:
                posicion = Direccion1.Ejecutar();
                valor = Direccion2.Ejecutar();
                ContenedorEstatico.InsertarEnStack(posicion.intValue(), valor);
                break;
            case PTRH:
                Double val = Direccion1.Ejecutar();
                Formulario.H = val.intValue();
                break;
            case PTRS:
                val = Direccion1.Ejecutar();
                Formulario.P = val.intValue();
                break;
            case TEMP:
                val = Direccion1.Ejecutar();
                ContenedorEstatico.SetTemporal(id,val);
                break;
        }
        return 0.0;
    }
    
}

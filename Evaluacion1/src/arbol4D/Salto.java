/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol4D;

import java.util.Objects;

/**
 *
 * @author eljul
 */
public class Salto extends Nodo4D{

    public static enum TipoSalto{
        JE,
        JNE,
        JG,
        JGE,
        JL,
        JLE,
        JMP
    }
    
    public TipoSalto tipo;
    public Nodo4D Direccion1;
    public Nodo4D Direccion2;
    public Nodo4D Resultado;

    public Salto(TipoSalto tipo, Nodo4D Direccion1, Nodo4D Direccion2, Nodo4D Resultado) {
        this.tipo = tipo;
        this.Direccion1 = Direccion1;
        this.Direccion2 = Direccion2;
        this.Resultado = Resultado;
    }
    
    public Salto(TipoSalto tipo, Nodo4D Resultado) {
        this.tipo = tipo;
        this.Direccion2 = null;
        this.Resultado = Resultado;
    }
    
    
    @Override
    public double Ejecutar() {
        if(tipo == TipoSalto.JMP){
            Resultado.Ejecutar();
            return 0.0;
        }
        Double val1 = Direccion1.Ejecutar();
        Double val2 = Direccion2.Ejecutar();
        boolean ejecutarSalto = false;
        switch(tipo){
            case JE:
                ejecutarSalto = (Objects.equals(val1, val2));
                break;
            case JGE:
                ejecutarSalto = (val1 >= val2);
                break;
            case JG:
                ejecutarSalto = (val1 > val2);
                break;
            case JL:
                ejecutarSalto = (val1 < val2);
                break;
            case JLE:
                ejecutarSalto = (val1 <= val2);
                break;
            case JNE:
                ejecutarSalto = (!Objects.equals(val1, val2));
                break;
        }
        if(ejecutarSalto){
            Resultado.Ejecutar();
        }
        return 0.0;
    }
    
}

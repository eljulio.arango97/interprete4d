/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol4D;

/**
 *
 * @author eljul
 */
public class SetTemp extends Nodo4D{
    public String id;
    public Nodo4D valor;
    
    public SetTemp(String id) {
        this.id = id;
    }

    @Override
    public double Ejecutar() {
        Double val = valAsignar;
        ContenedorEstatico.SetTemporal(id, val);
        return 0.0;
    }
}

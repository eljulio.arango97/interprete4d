/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol4D;

import evaluacion1.Formulario;

/**
 *
 * @author eljul
 */
public class Imprimir extends Nodo4D{
    public TipoImpresion tipo;
    public Nodo4D valor;

    public Imprimir(TipoImpresion tipo, Nodo4D valor) {
        this.tipo = tipo;
        this.valor = valor;
    }

    @Override
    public double Ejecutar() {
        double a = valor.Ejecutar();;
        if (tipo == TipoImpresion.CARACTER) {
            if(a < 256){
                int b = (int)a;
                char val = (char)b;
                Formulario.ImprimirConsola(val + "");
            } 
        } else if (tipo == TipoImpresion.ENTERO) {
            int b = (int)a;
            Formulario.ImprimirConsola(a + "");
        } else {
            Formulario.ImprimirConsola(a + "");
        }
        return 0.0;
    }
}

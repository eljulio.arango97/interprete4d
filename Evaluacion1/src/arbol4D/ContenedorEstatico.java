/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol4D;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;
import java.util.Stack;

/**
 *
 * @author eljul
 */
public class ContenedorEstatico {
    // Estructuras en tiempo de ejecucion
    public static ArrayList<Double> Stack = new ArrayList<>();
    public static ArrayList<Double> Heap = new ArrayList<>();
    public static ArrayList<NodoAuxiliar> Auxiliar = new ArrayList<>();
    public static LinkedList<NodoTemporal> Temporales = new LinkedList<>();
    public static boolean Incremento = true;
    
    public static Hashtable<Integer, Nodo4D> EstructuraDasm = new Hashtable<>();
    public static int LineaActual = 0;
    
    public static Stack<Integer> PilaRetornos = new Stack<>();
    
    
    public static double ObtenerTemporal(String temp){
        for (NodoTemporal tem: Temporales) {
            if(temp.equalsIgnoreCase(tem.temp)){
                return tem.valor;
            }
        }
        return 0.0;
    }
    
    public static double SetTemporal(String temp, double valor){
        for (NodoTemporal tem: Temporales) {
            if(temp.equalsIgnoreCase(tem.temp)){
                tem.valor = valor;
                return 1;
            }
        }
        Temporales.add(new NodoTemporal(valor, temp));
        return 0.0;
    }
    
    public static void pushStack(Double valor){
        Stack.add(valor);
    }
    
    public static void pushHeap(Double valor){
        Heap.add(valor);
    }
    
    public static void InsertarAuxiliar(String Nombre, int posicion){
        Auxiliar.add(new NodoAuxiliar(posicion, Nombre));
    }
    
    
    public static Double PopStack(int pos){
        if(pos < Stack.size()){
            return Stack.get(pos);
        }else{
            return 0.0;
        }
    }
    
    public static Double PopHeap(int pos){
        if(pos < Heap.size()){
            return Heap.get(pos);
        }else{
            return 0.0;
        }
    }
    
    public static int ObtenerPosicion(String nombre, int fila, int columna){
        if(Auxiliar.size() > 0){
            for(int i = 0; i < Auxiliar.size(); i++){
                if(Auxiliar.get(i).Nombre.equals(nombre)){
                    return Auxiliar.get(i).posicion;
                }
            }
        }
        //Draco.AgregarError("Llamada Funcion", "Funcion no declarada", fila, columna, "Semantico");
        return 0;
    }
    
    public static void IncrementarStack(int tam){
        while(Stack.size() <= tam){
            Stack.add(0.0);
        }
    }
    
    public static void IncrementarHeap(int tam){
        while(Heap.size() <= tam){
            Heap.add(0.0);
        }
    }
    
    public static void InsertarEnStack(int pos, Double valor){
        IncrementarStack(pos);
        Stack.set(pos, valor);
    }
    
    public static void InsertarEnHeap(int pos, Double valor){
        IncrementarHeap(pos);
        Heap.set(pos, valor);
    }
    
}

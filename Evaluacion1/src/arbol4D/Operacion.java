/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol4D;

/**
 *
 * @author eljul
 */
public class Operacion extends Nodo4D{

    public enum TipoOpe {
        SUMA, 
        RESTA,
        MULTIPLICACION, 
        DIVISION, 
        MODULAR
    }
    
    public TipoOpe tipo;
    public Nodo4D direccion1;
    
    public Nodo4D direccion2;
    public Nodo4D resultado;

    public Operacion(TipoOpe tipo, Nodo4D direccion1, Nodo4D direccion2, Nodo4D resultado) {
        this.tipo = tipo;
        this.direccion1 = direccion1;
        this.direccion2 = direccion2;
        this.resultado = resultado;
    }
    
    @Override
    public double Ejecutar() {
        Double val1 = direccion1.Ejecutar();
        Double val2 = direccion2.Ejecutar();
        switch(tipo){
            case SUMA:
                resultado.valAsignar = val1 + val2;
                resultado.Ejecutar();
            case DIVISION:
                resultado.valAsignar = val1 / val2;
                resultado.Ejecutar();
            case MODULAR:
                resultado.valAsignar = val1 % val2;
                resultado.Ejecutar();
            case MULTIPLICACION:
                resultado.valAsignar = val1 * val2;
                resultado.Ejecutar();
            case RESTA:
                resultado.valAsignar = val1 - val2;
                resultado.Ejecutar();
        }
        return 0.0;
    }
    
    
}

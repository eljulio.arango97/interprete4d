/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol4D;

import evaluacion1.Formulario;

/**
 *
 * @author eljul
 */
public class Direccion extends Nodo4D{
    public String valorTemp;
    public TipoDirec tipo;
    
    public Direccion( TipoDirec a1, String a){
        valorTemp = a;
        this.tipo = a1;
    }
   
    
    public Direccion(TipoDirec a1){
        tipo = a1;
    }

    @Override
    public double Ejecutar() {
        if(tipo == TipoDirec.PHEAP){
            return Formulario.H;
        }
        if(tipo == TipoDirec.PSTACK){
            return Formulario.H;
        }
        if(tipo == TipoDirec.NUMERO){
            return Double.parseDouble(valorTemp);
        }else{
            return ContenedorEstatico.ObtenerTemporal(valorTemp);
        }
    }
    
    public enum TipoDirec{
        NUMERO, 
        TEMPORAL, 
        PSTACK, 
        PHEAP
    }
    
    
}

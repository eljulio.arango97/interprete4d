/*------------  1ra Area: Codigo de Usuario ---------*/
//------> Paquetes,importaciones
package Analizador;
import java_cup.runtime.*;
import java.util.LinkedList;

/*------------  2da Area: Opciones y Declaraciones ---------*/
%%
%{
    //----> Codigo de usuario en sintaxis java
    // public static LinkedList<TError> TablaEL = new LinkedList<TError>(); 
%}

//-------> Directivas
%public 
%class A_Lexico
%cupsym Simbolos
%cup
%ignorecase
%char
%column
%full
%line
%unicode

//------> ER de palabras reservadas
JMP = "jmp"
JE = "je"
JNE = "jne"
JG = "jg"
JL = "jl"
JGE = "jge"
JLE = "jle"
BEGIN = "begin"
END = "end"
CALL = "call"
PRINT = "print"
TIPOC = "c"
TIPOE = "e"
TIPOD = "d"
STACK = "stack"
HEAP = "heap"
PTRP = "P"
PTRH = "H"


//------> Expresiones Regulares
decimal = [-]?[0-9]+(".")[0-9]+
numero = [-]?[0-9]+
idTemp = T[0-9]+
idEtq = L[0-9]+
identificador = ["_"0-9A-Za-zñÑ]+

//------> Estados
%state COMENT_MULTI
%state COMENT_SIMPLE
%%
/*------------  3ra Area: Reglas Lexicas ---------*/
<YYINITIAL>  "/*"               {yybegin(COMENT_MULTI);}
<COMENT_MULTI>    "*/"          {yybegin(YYINITIAL);}
<COMENT_MULTI>    .             {}
<COMENT_MULTI>    [\t\r\n\f]    {}

<YYINITIAL>  "//"                 {yybegin(COMENT_SIMPLE);}
<COMENT_SIMPLE>    [^"\n"]        {}
<COMENT_SIMPLE>    "\n"           {yybegin(YYINITIAL);}

/*------------  3ra Area: Reglas Lexicas ---------*/
//-------------> Simbolos XRE
<YYINITIAL> "+"             { System.out.println("Reconocio "+yytext()+" Reservada Mas"); return new Symbol(Simbolos.mas, yycolumn, yyline, yytext()); }
<YYINITIAL> "-"             { System.out.println("Reconocio "+yytext()+" Reservada menos"); return new Symbol(Simbolos.menos, yycolumn, yyline, yytext()); }
<YYINITIAL> "*"             { System.out.println("Reconocio "+yytext()+" Reservada por"); return new Symbol(Simbolos.por, yycolumn, yyline, yytext()); }
<YYINITIAL> "/"             { System.out.println("Reconocio "+yytext()+" Reservada div"); return new Symbol(Simbolos.div, yycolumn, yyline, yytext()); }
<YYINITIAL> ","             { System.out.println("Reconocio "+yytext()+" Reservada coma"); return new Symbol(Simbolos.coma, yycolumn, yyline, yytext()); }
<YYINITIAL> "%"             { System.out.println("Reconocio "+yytext()+" Reservada mod"); return new Symbol(Simbolos.mod, yycolumn, yyline, yytext()); }
<YYINITIAL> "="             { System.out.println("Reconocio "+yytext()+" Reservada igual"); return new Symbol(Simbolos.igual, yycolumn, yyline, yytext()); }
<YYINITIAL> "("             { System.out.println("Reconocio "+yytext()+" Reservada parabre"); return new Symbol(Simbolos.parabre, yycolumn, yyline, yytext()); }
<YYINITIAL> ")"             { System.out.println("Reconocio "+yytext()+" Reservada parcierra"); return new Symbol(Simbolos.parcierra, yycolumn, yyline, yytext()); }
<YYINITIAL> ":"             { System.out.println("Reconocio "+yytext()+" Reservada dos puntos"); return new Symbol(Simbolos.dospuntos, yycolumn, yyline, yytext()); }

//-------------> Palabras Reservadas
<YYINITIAL> {JMP}            { System.out.println("Reconocio "+yytext()+" Reservada JMP"); return new Symbol(Simbolos.jmp, yycolumn, yyline, yytext()); }
<YYINITIAL> {JE}             { System.out.println("Reconocio "+yytext()+" Reservada JE"); return new Symbol(Simbolos.je, yycolumn, yyline, yytext()); }
<YYINITIAL> {JNE}            { System.out.println("Reconocio "+yytext()+" Reservada JNE"); return new Symbol(Simbolos.jne, yycolumn, yyline, yytext()); }
<YYINITIAL> {JG}             { System.out.println("Reconocio "+yytext()+" Reservada JG"); return new Symbol(Simbolos.jg, yycolumn, yyline, yytext()); }
<YYINITIAL> {JL}             { System.out.println("Reconocio "+yytext()+" Reservada JL"); return new Symbol(Simbolos.jl, yycolumn, yyline, yytext()); }
<YYINITIAL> {JGE}            { System.out.println("Reconocio "+yytext()+" Reservada JGE"); return new Symbol(Simbolos.jge, yycolumn, yyline, yytext()); }
<YYINITIAL> {JLE}            { System.out.println("Reconocio "+yytext()+" Reservada JLE"); return new Symbol(Simbolos.jle, yycolumn, yyline, yytext()); }
<YYINITIAL> {BEGIN}          { System.out.println("Reconocio "+yytext()+" Reservada BEGIN"); return new Symbol(Simbolos.begin, yycolumn, yyline, yytext()); }
<YYINITIAL> {END}            { System.out.println("Reconocio "+yytext()+" Reservada END"); return new Symbol(Simbolos.end, yycolumn, yyline, yytext()); }
<YYINITIAL> {CALL}           { System.out.println("Reconocio "+yytext()+" Reservada CALL"); return new Symbol(Simbolos.call, yycolumn, yyline, yytext()); }
<YYINITIAL> {PRINT}          { System.out.println("Reconocio "+yytext()+" Reservada PRINT"); return new Symbol(Simbolos.print, yycolumn, yyline, yytext()); }
<YYINITIAL> {TIPOC}          { System.out.println("Reconocio "+yytext()+" Reservada TIPOC"); return new Symbol(Simbolos.tipoc, yycolumn, yyline, yytext()); }
<YYINITIAL> {TIPOD}          { System.out.println("Reconocio "+yytext()+" Reservada TIPOD"); return new Symbol(Simbolos.tipod, yycolumn, yyline, yytext()); }
<YYINITIAL> {TIPOE}          { System.out.println("Reconocio "+yytext()+" Reservada TIPOE"); return new Symbol(Simbolos.tipoe, yycolumn, yyline, yytext()); }
<YYINITIAL> {STACK}          { System.out.println("Reconocio "+yytext()+" Reservada STACK"); return new Symbol(Simbolos.stack, yycolumn, yyline, yytext()); }
<YYINITIAL> {HEAP}           { System.out.println("Reconocio "+yytext()+" Reservada HEAP"); return new Symbol(Simbolos.heap, yycolumn, yyline, yytext()); }
<YYINITIAL> {PTRP}           { System.out.println("Reconocio "+yytext()+" Reservada P"); return new Symbol(Simbolos.ptrs, yycolumn, yyline, yytext()); }
<YYINITIAL> {PTRH}           { System.out.println("Reconocio "+yytext()+" Reservada H"); return new Symbol(Simbolos.ptrh, yycolumn, yyline, yytext()); }

//------------> Tipos de Elementos
<YYINITIAL> {idTemp}            { System.out.println("Reconocio "+yytext()+" Reservada Temporal"); return new Symbol(Simbolos.temp, yycolumn, yyline, yytext()); }
<YYINITIAL> {idEtq}             { System.out.println("Reconocio "+yytext()+" Reservada Etiqueta"); return new Symbol(Simbolos.etq, yycolumn, yyline, yytext()); }
<YYINITIAL> {decimal}           { System.out.println("Reconocio "+yytext()+" Reservada Decimal"); return new Symbol(Simbolos.decimal, yycolumn, yyline, yytext()); }
<YYINITIAL> {numero}            { System.out.println("Reconocio "+yytext()+" Reservada Numero"); return new Symbol(Simbolos.numero, yycolumn, yyline, yytext()); }
<YYINITIAL> {identificador}     { System.out.println("Reconocio "+yytext()+" Reservada Identificador"); return new Symbol(Simbolos.id, yycolumn, yyline, yytext()); }

//------> Espacios
[ \t\r\n\f]            {/* Espacios en blanco, se ignoran */}


//------> Errores Lexicos
.                       { //Draco.AgregarError(yytext(), "Caracter no valido", yyline, yycolumn, "Lexico");
                          }





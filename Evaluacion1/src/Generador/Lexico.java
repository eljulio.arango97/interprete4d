/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Generador;

import java.io.File;

/**
 *
 * @author Usuario
 */
public class Lexico {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String path="src/Analizador/A_Lexico.jflex";
        GenerarLexico(path);
    }
    
    public static void GenerarLexico(String path){
        File file=new File(path);
        jflex.Main.generate(file);
    }
}
